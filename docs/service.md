# Service

Gaia-X will define a 3-level taxonomy for services. This taxonomy is being worked on by the Gaia-X Working Groups and will be published in a separate document.

```mermaid
classDiagram
    ServiceOffering o-- Resource
    Resource o-- Resource
    Resource <|-- PhysicalResource
    Resource <|-- VirtualResource
    VirtualResource <|-- InstantiatedVirtualResource

    ServiceOffering ..> "1" Participant : service_provider
    Resource ..> "1..*" Participant : resource_maintainer

    PhysicalResource ..> "0..*" Participant : resource_owner
    PhysicalResource ..> "0..*" Participant : resource_manufacturer
    VirtualResource ..> "1..*" Participant : copyright_owner

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
    InstantiatedVirtualResource ..> "1..*" Participant : tenant_owner
``` 

## Service offering

This is the generic format for all service offerings

| Version | Attribute               | Card. | Signed by | Comment                                         |
|---------|-------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `service_provider`      | 1     | State     | a `provider`'s URL which provide the service    |
| 1.0     | `resource[]`            | 0..*  | State     | a list of `resource` used by the service        |
| 1.1     | `terms_and_conditions[]`| 1..*  | State     | a list of `terms_and_conditions`  |
| 1.1     | `policies[]`            | 0..*  | State     | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) |


### Service offering classes

While a more complete description of service offerings taxonomy is defined by the Service Characteristics Working Group and will be integrated inside that document, some services must already be specified to enable building a common governance across ecosystems.

```mermaid
flowchart BT
    so[Service Offering]
    dec[Data Exchange Connector]
    dec --> so

    wallet[Verifiable Credential Wallet]
    wallet --> so
``` 

### Data exchange connector service


A `data connector` offering, such as and not limited to, a service bus, a messaging or an event streaming platform, must fulfil those requirements:

<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Version | Attribute               | Card. | Signed by | Comment                                         |
|---------|-------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `physical_resource[]`   | 1..*  | State     | a list of `resource` with information of where is the data located. |
| 1.0     | `virtual_resource[]`    | 1..*  | State     | a list of `resource` with information of who owns the data. |
| 1.1     | `policies[]`            | 1..*  | State     | a list of `policy` expressed using a DSL used by the data connector policy engine leveraging Gaia-X Self-descriptions as both data inputs and policy inputs |


### Verifiable Credential Wallet

A `wallet` enables to store, manage, present Verifiable Credentials:
<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Version | Attribute                               | Card. | Signed by | Comment                                         |
|---------|-----------------------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `verifiable_credential_export_format[]` | 1..*  | State     | a list of machine readable format used to export verifiable credentials. |
| 1.0     | `private_key_export_format[]`           | 1..*  | State     | a list of machine readable format used to export private keys. |

## Resource

A resource aggregates with Service Offering.

| Version | Attribute               | Card. | Signed by | Comment                                         |
|---------|-------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `resource[]`            | 0..*  | State     | a list of `resource` used by the resource       |
| 1.0     | `resource_maintainer[]` | 1..*  | State     | a list of `participant` maintaining the resource in operational condition. |

### Physical Resource

A Physical Resouce inherits from a Resource.  
A Physical resource is, and not limited to, a datacenter, a baremetal service, a warehouse, a plant. Those are entities that have a weigth and position in our space.

| Version | Attribute              | Card. | Signed by | Comment                                     |
|---------|------------------------|-------|-----------|---------------------------------------------|
| 1.0     | `resource_owner[]`     | 0..*  | State     | a list of `participant` owning the resource. |
| 1.0     | `resource_manufacturer[]` | 0..*  | State     | a list of `participant` manufacturing the resource. |
| 1.0     | `location[].country`   | 1..*  | State     | a list of physical location in ISO 3166-1 alpha2, alpha-3 or numeric format. |
| 1.0     | `location[].gps`       | 0..*  | State     | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

### Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model.

| Version | Attribute            | Card. | Signed by | Comment                                   |
|---------|----------------------|-------|-----------|-------------------------------------------|
| 1.0     | `copyright_owner[]`  | 1..*  | State     | a list of copyright owner                 |
| 1.0     | `license[]`          | 1..*  | State     | a list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) license identifiers or URL to license document |

### Instantiated Virtual Resource

An Instantiated Virtual Resource inherits from a Virtual Resource.  
An Instantiated Virtual resource is a running resource exposing endpoints such as, and not limited to, a running process, an online API, a network connection, a virtual machine, a container, an operating system.

| Version | Attribute            | Card. | Signed by | Comment                                   |
|---------|----------------------|-------|-----------|-------------------------------------------|
| 1.0     | `tenant_owner[]`        | 1..*  | State     | a list of `participant` with contractual relation with the resource. |
| 1.x     | `virtual_location[]` | 1..*  | State     | a list of location such an Availability Zone, network segment, IP range, AS number, ... (format to be specified in a separated table) |
| 1.x     | `endpoint[]`         | 1..*  | State     | a list of exposed endpoints as defined in [ISO/IEC TR 23188:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:tr:23188:ed-1:v1:en:term:3.1.7) |
