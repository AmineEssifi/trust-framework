# Gaia-X Trust Framework

For Gaia-X to ensure a higher and unprecedented level of trust in digital platforms, we need to make trust an easy to understand and adopted principle. For this reason, Gaia-X developed a Trust Framework – formerly known as Gaia-X Compliance - and Labelling Framework that safeguards data protection, transparency, security, portability, and flexibility for the ecosystem as well as sovereignty and European Control.

The Trust Framework is the set of rules that define the minimum baseline to be part of the **Gaia-X Ecosystem**. Those rules ensure a common governance and the basic levels of interoperability across individual ecosystems while letting the users in full control of their choices.[^digitalsov]

[^digitalsov]: <https://www.europarl.europa.eu/RegData/etudes/BRIE/2020/651992/EPRS_BRI(2020)651992_EN.pdf>

In other words, the Gaia-X Ecosystem is the virtual set of participants and service offerings following the Gaia-X requirements from the Gaia-X Trust Framework.  
The Trust Framework uses verifiable credentials and linked data representation to build a **FAIR knowledge graph of verifiable claims** from which additional **trust and composability indexes** can be automatically computed. 

The set of computable rules known as compliance process is automated and versionned. It means that this document will also be versionned.

## Trust Framework scope

Those rules apply to **all** Gaia-X Self-Description files and there is a Self-Description files for **all** the entities defined as part of the Gaia-X Conceptual model described in the Gaia-X Architecture document:

This list mainly consists of:

- Participant with Consumer, Federator, Provider
- Service Offering
- Resource

### Gaia-X Labels

The Labelling Framework itself is further detailed and translated into concrete criteria and measures in the [Gaia-X Labelling Criteria document](https://gaia-x.eu/sites/default/files/2022-02/Labelling_Criteria_Whitepaper_v07.pdf).

| Framework           | Notes |
|---------------------|-------|
| Trust Framework     | Compulsory set of rules to comply with in other to be part of the Gaia-X Ecosystem. <br/> Individual ecosystems can extend those rules. |
| Labelling Framework | Optional set of criteria for Service Offerings. | 

## Gaia-X Self-Description

Gaia-X Self-Description files are:

- machine readable text file
- cryptographically signed file preventing tampering with its content
- using link-data to describe attributes

The format is following the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/).

## Gaia-X Trust Framework

There are 4 types of rules:

- serialization format and syntax.
- cryptographic signature validation and validation of the keypair associated identity.
- attribute value consistency.
- attribute veracity verification.
